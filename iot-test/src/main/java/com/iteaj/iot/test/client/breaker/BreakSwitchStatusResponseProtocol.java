package com.iteaj.iot.test.client.breaker;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.protocol.ServerInitiativeProtocol;
import com.iteaj.iot.test.BreakerProtocolType;

public class BreakSwitchStatusResponseProtocol extends ServerInitiativeProtocol<BreakerClientMessage> {

    public BreakSwitchStatusResponseProtocol(BreakerClientMessage requestMessage) {
        super(requestMessage);
    }

    @Override
    protected void doBuildRequestMessage(BreakerClientMessage requestMessage) {

    }

    @Override
    protected BreakerClientMessage doBuildResponseMessage() {
        return null;
    }

    @Override
    public ProtocolType protocolType() {
        return BreakerProtocolType.SwitchStatus;
    }
}
