package com.iteaj.iot.server.component;

import com.iteaj.iot.codec.adapter.SimpleChannelDecoderAdapter;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.ServerMessage;

public abstract class SimpleChannelDecoderServerComponent<M extends ServerMessage> extends TcpDecoderServerComponent<M> {

    public SimpleChannelDecoderServerComponent(ConnectProperties connectProperties) {
        super(connectProperties);
    }

    @Override
    public SimpleChannelDecoderAdapter getMessageDecoder() {
        return new SimpleChannelDecoderAdapter(this);
    }

}
