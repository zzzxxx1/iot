package com.iteaj.iot.server.udp.impl;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.component.DatagramPacketDecoderServerComponent;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;

public class DefaultUdpServerComponent extends DatagramPacketDecoderServerComponent<DefaultUdpServerMessage> {

    public DefaultUdpServerComponent(ConnectProperties config) {
        super(config);
    }

    @Override
    public String getDesc() {
        return "UDP协议IOT默认实现";
    }

    @Override
    protected ClientInitiativeProtocol<DefaultUdpServerMessage> doGetProtocol(DefaultUdpServerMessage message, ProtocolType type) {
        return new DefaultUdpServerProtocol(message);
    }

    @Override
    public String getName() {
        return "UDP(默认)";
    }

    @Override
    public Class<DefaultUdpServerMessage> getMessageClass() {
        return DefaultUdpServerMessage.class;
    }

    @Override
    public DefaultUdpServerMessage createMessage(byte[] message) {
        return new DefaultUdpServerMessage(message);
    }
}
