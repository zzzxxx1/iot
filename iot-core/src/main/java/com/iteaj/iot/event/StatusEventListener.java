package com.iteaj.iot.event;

import com.iteaj.iot.FrameworkComponent;

/**
 * 客户端状态事件监听
 * @see StatusEvent
 * @see ClientStatus
 */
public interface StatusEventListener<S, C extends FrameworkComponent> extends FrameworkEventListener<StatusEvent> {

    @Override
    default boolean isMatcher(IotEvent event) {
        return event instanceof StatusEvent;
    }

    default void onEvent(StatusEvent event) {
        if(event == null) {
            throw new IllegalArgumentException("未指定设备事件参数");
        }

        switch (event.getStatus()) {
            case online:
                online((S) event.getSource(), (C) event.getComponent());
                break;
            case offline:
                offline((S) event.getSource(), (C) event.getComponent());
                break;

            default: throw new IllegalStateException("错误的设备事件类型: " + event.getStatus());
        }
    }

    /**
     * 注册成功处理
     * @param source
     */
    void online(S source, C component);

    /**
     * 连接断开处理
     * @param source
     */
    void offline(S source, C component);
}
