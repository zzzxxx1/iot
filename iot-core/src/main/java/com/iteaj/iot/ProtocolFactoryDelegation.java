package com.iteaj.iot;

public class ProtocolFactoryDelegation<M extends SocketMessage> extends SocketProtocolFactory<M> {

    private IotProtocolFactory iotProtocolFactory;

    public ProtocolFactoryDelegation(IotProtocolFactory iotProtocolFactory,ProtocolTimeoutStorage delegation) {
        super(delegation);
        this.iotProtocolFactory = iotProtocolFactory;
    }

    @Override
    public AbstractProtocol getProtocol(M message) {
        return iotProtocolFactory.getProtocol(message);
    }

    @Override
    protected AbstractProtocol<M> doGetProtocol(M message, ProtocolType type) {
        return null;
    }
}
