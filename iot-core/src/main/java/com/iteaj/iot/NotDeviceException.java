package com.iteaj.iot;

/**
 * 设备不存在
 */
public class NotDeviceException extends FrameworkException {

    public static NotDeviceException DEFAULT = new NotDeviceException();

    public static NotDeviceException getInstance() {
        return DEFAULT;
    }

    public NotDeviceException() {
        this("无此设备或不在线");
    }

    public NotDeviceException(String message) {
        super(message);
    }

    public NotDeviceException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotDeviceException(Throwable cause) {
        super(cause);
    }

    public NotDeviceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
