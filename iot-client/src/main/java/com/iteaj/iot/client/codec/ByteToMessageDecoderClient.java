package com.iteaj.iot.client.codec;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.TcpSocketClient;
import com.iteaj.iot.client.component.TcpClientComponent;
import com.iteaj.iot.codec.adapter.ByteToMessageDecoderAdapter;

public class ByteToMessageDecoderClient extends TcpSocketClient {

    public ByteToMessageDecoderClient(TcpClientComponent clientComponent, ClientConnectProperties config) {
        super(clientComponent, config);
    }

    @Override
    protected ByteToMessageDecoderAdapter createProtocolDecoder() {
        return new ByteToMessageDecoderAdapter(this.getClientComponent());
    }
}
