package com.iteaj.iot.client;

import com.iteaj.iot.config.ConnectProperties;

/**
 * 创建客户端
 */
public interface ClientFactory {

    IotClient createNewClient(ConnectProperties config);
}
